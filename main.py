# https://machinelearningmastery.com/extra-trees-ensemble-with-python/
# https://link.springer.com/content/pdf/10.1007/s10994-006-6226-1.pdf
# https://machinelearningmastery.com/information-gain-and-mutual-information/

from typing import Optional

import pandas as pd
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
import numpy as np

from criterion import crossentropy_loss
from decision_tree import DecisionTree, Node, ExtraTree, LearningType


def load_dataset() -> tuple:
    data = load_iris()
    X, y, column_names = data['data'], data['target'], data['feature_names']
    X = pd.DataFrame(X, columns=column_names)
    X['target'] = y
    X, y = X.drop(columns='target'), X['target']
    X_train, X_val, y_train, y_val = train_test_split(X, y, random_state=8)

    return X_train, X_val, y_train, y_val, column_names


def print_nodes(node: Optional[Node], type: str) -> None:
    if node is None:
        print(f'{type} branch is empty.')
        return
    print(
        f'Split in {node.column} (threshold {node.threshold}) with info gain: {node.info_gain}. This is {type} branch.')

    print_nodes(node.left, 'left')
    print_nodes(node.right, 'right')


def main():
    X_train, X_val, y_train, y_val, column_names = load_dataset()

    max_depth = 20
    min_leaf_size = 3
    max_nodes = 1000
    classes = np.unique(y_train)
    criterion = crossentropy_loss

    decision_tree = DecisionTree(max_nodes, max_depth, criterion, classes)
    nodes = decision_tree.build(X_train, y_train, column_names)
    print_nodes(nodes, 'root')


def main_extra_tree():
    X_train, X_val, y_train, y_val, column_names = load_dataset()

    n_features = 2
    min_samples = 3
    n_estimators = 1

    decision_tree = ExtraTree(n_estimators, n_features, min_samples)
    nodes = decision_tree.train(X_train, y_train, column_names, LearningType.CLASSIFICATION)

    # print_nodes(nodes, 'root')
    print(nodes)


if __name__ == '__main__':
    main_extra_tree()

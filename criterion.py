import numpy as np


def crossentropy_loss(samples: np.array, classes: list, y: np.array) -> float:
    p_hat = calc_p_hat(samples, classes, y)
    total_sum = 0
    for p in p_hat:
        total_sum += p * np.log2(p)
    return total_sum


def gini(samples: np.array, classes: list, y: np.array) -> float:
    p_hat = calc_p_hat(samples, classes, y)
    total_sum = 0
    for p in p_hat:
        total_sum += p * (1 - p)
    return total_sum


def calc_p_hat(values: np.array, classes: list, y: np.array) -> np.array:
    p_hat = []
    for c in classes:
        p_hat.append(len(values[y == c]) / len(values))
    return np.array(p_hat)


def classification_score(S: np.array, y, split: float, classes: list) -> float:
    left_split = S[S < split]
    right_split = S[S >= split]
    first_class = len(left_split) / len(S)
    second_class = len(right_split) / len(S)
    h_c_s = -(first_class * np.log2(first_class) + second_class * np.log2(second_class))
    h_s_s = crossentropy_loss(S, classes, y)
    i_s_c = compute_mutual_information(left_split, right_split)

    return 2 * i_s_c / (h_c_s + h_s_s)


def regression_score(S: np.array, split: float) -> float:
    left = S[S < split]
    right = S[S >= split]
    n = len(S)
    S_var = np.var(S)
    left_var = np.var(left)
    right_var = np.var(right)
    n_left = len(left)
    n_right = len(right)

    return (S_var - ((n_left / n) * left_var) - ((n_right / n) * right_var)) / S_var


def compute_mutual_information(x: np.array, y: np.array):
    sum_mi = 0.0
    x_value_list = np.unique(x)
    y_value_list = np.unique(y)
    Px = np.array([len(x[x == xval]) / float(len(x)) for xval in x_value_list])  # P(x)
    Py = np.array([len(y[y == yval]) / float(len(y)) for yval in y_value_list])  # P(y)
    for i in range(len(x_value_list)):
        if Px[i] == 0.:
            continue
        sy = y[x == x_value_list[i]]
        if len(sy) == 0:
            continue
        pxy = np.array([len(sy[sy == yval]) / float(len(y)) for yval in y_value_list])  # p(x,y)
        t = pxy[Py > 0.] / Py[Py > 0.] / Px[i]  # log(P(x,y)/( P(x)*P(y))
        sum_mi += sum(pxy[t > 0] * np.log2(t[t > 0]))  # sum ( P(x,y)* log(P(x,y)/( P(x)*P(y)) )
    return sum_mi

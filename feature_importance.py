from sklearn.datasets import load_digits
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.decomposition import PCA
import numpy as np

def main():
    X, y = load_digits(return_X_y=True)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state = 42)
    clf = DecisionTreeClassifier(max_depth=10, random_state=8)
    clf.fit(X_train, y_train)

    print("---------------")
    print(f"Score: {clf.score(X_test, y_test)}")
    print("")
    print("")
    print(f"Feature Importance: {clf.feature_importances_}")
    print(f"Highest features index: {np.argmax(clf.feature_importances_)}")
    print("---------------")

    pca = PCA()
    scaler = StandardScaler()
    X_pca = pca.fit_transform(scaler.fit_transform(X))
    X_train, X_test, y_train, y_test = train_test_split(X_pca, y, test_size=0.33, random_state=42)
    clf = RandomForestClassifier(random_state=8)
    clf.fit(X_train, y_train)

    print("---------------")
    print(f"Score: {clf.score(X_test, y_test)}")
    print("")
    print("")
    print(f"Feature Importance: {clf.feature_importances_}")
    print(f"Highest features index: {np.argmax(clf.feature_importances_)}")
    print("---------------")


if __name__ == '__main__':
    main()
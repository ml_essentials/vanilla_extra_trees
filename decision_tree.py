from __future__ import annotations
from dataclasses import dataclass
from enum import Enum
from typing import Optional

import numpy as np
from copy import deepcopy

import pandas as pd

from criterion import classification_score, regression_score


@dataclass
class Node:
    threshold: float
    probas: float
    info_gain: float
    column: str
    right: Optional[Node] = None
    left: Optional[Node] = None


class BestSplitNotFoundError(Exception):
    pass


class LearningType(Enum):
    CLASSIFICATION = 1
    REGRESSION = 2


class DecisionTree:
    def __init__(self, max_nodes, max_depth, criterion, classes):
        self.classes = classes
        self.criterion = criterion
        self.max_depth = max_depth
        self.max_nodes = max_nodes

        self.nodes_count_ = 0
        self.depth_count_ = 0

    def build(self, X_train: pd.DataFrame, y_train: pd.DataFrame, column_names: list) -> Optional[Node]:
        return self._build_decision_tree(X_train, y_train, column_names)

    def _build_decision_tree(self, samples: pd.DataFrame, y_train: pd.DataFrame, column_names: list) -> Optional[Node]:
        if self.nodes_count_ == self.max_nodes or self.depth_count_ == self.max_depth:
            return

        try:
            node = self._build_node(samples, y_train, column_names)
        except BestSplitNotFoundError:
            return None

        self.nodes_count_ += 1
        column_names.remove(node.column)

        self.depth_count_ += 1
        node.left = self._build_decision_tree(samples[samples[node.column] < node.threshold], y_train, deepcopy(column_names))
        node.right = self._build_decision_tree(samples[samples[node.column] > node.threshold], y_train, deepcopy(column_names))
        self.depth_count_ -= 1

        return node

    def _build_node(self, samples: pd.DataFrame, y_train: pd.DataFrame, column_names: list) -> Node:
        parent_loss = self.criterion(samples, self.classes, y_train)
        info_gain, column, threshold = self._find_best_split(samples, y_train, parent_loss, column_names)
        return Node(column=column, threshold=threshold, probas=self.criterion(samples[column], self.classes, y_train),
                    info_gain=info_gain)

    def _find_best_split(self, samples: pd.DataFrame, y_train: pd.DataFrame, parent_loss: float,
                         column_names: list) -> tuple:
        best_split_params = (0, 'init', 0)
        for name in column_names:
            for value in samples[name]:
                threshold = value
                current_loss = parent_loss - self._calc_loss(samples[name], y_train, threshold)
                if current_loss > best_split_params[0]:
                    best_split_params = (
                        current_loss, name, threshold
                    )
        if best_split_params[1] == 'init':
            raise BestSplitNotFoundError()

        return best_split_params

    def _calc_loss(self, values, y_train, threshold):
        l_values, r_values = values[values < threshold], values[values > threshold]
        if len(l_values) == 0 or len(r_values) == 0:
            return 1000

        l_node = self.criterion(l_values, self.classes, y_train)
        r_node = self.criterion(r_values, self.classes, y_train)
        return (l_node * len(l_values)) + (r_node * len(r_values))


class RandomDecisionTree:
    def __init__(self, n_min, classes, k, type: LearningType):
        self.type = type
        self.n_min = n_min
        self.k = k
        self.classes = classes

    def build(self, X_train: pd.DataFrame, y_train: pd.DataFrame, column_names: list) -> Optional[Node]:
        return self._build_decision_tree(X_train, y_train, column_names)

    def _build_decision_tree(self, samples: pd.DataFrame, y_train: pd.DataFrame, column_names: list) -> Optional[Node]:
        if len(samples) < self.n_min or len(column_names) <= 0:
            return None
        # – If all attributes are constant in S, then return TRUE; what does it mean?
        node = self._build_node(samples, y_train, column_names)

        column_names.remove(node.column)  # is it good ?

        node.left = self._build_decision_tree(samples[samples[node.column] < node.threshold], y_train, deepcopy(column_names))
        node.right = self._build_decision_tree(samples[samples[node.column] >= node.threshold], y_train, deepcopy(column_names))

        return node

    def _build_node(self, samples: pd.DataFrame, y_train: pd.DataFrame, column_names: list) -> Node:
        column_names = np.random.choice(column_names, size=self.k)
        best_feature, s_star = self._pick_best_random_split(samples, y_train, column_names)
        return Node(column=best_feature, threshold=s_star, probas=0, info_gain=0)

    def _pick_best_random_split(self, samples: pd.DataFrame, y_train: pd.DataFrame, column_names: list) -> tuple:
        splits = []
        for name in column_names:
            S = samples[name]
            a_s_min = float(np.min(S))
            a_s_max = float(np.max(S))
            a_c = np.random.uniform(a_s_min, a_s_max)
            splits.append(self._score(samples, y_train, a_c))

        best_index = np.argmax(splits)
        return column_names[best_index], splits[best_index]

    def _score(self, samples: pd.DataFrame, y_train: pd.DataFrame, a_c: float) -> float:
        if self.type is LearningType.CLASSIFICATION:
            return classification_score(samples, y_train, a_c, self.classes)
        return regression_score(samples, a_c)


class ExtraTree:
    def __init__(self, n_estimators: int, n_features: int, min_samples: int):
        self.min_samples = min_samples
        self.n_features = n_features
        self.n_estimators = n_estimators
        self.trees_ = []

    def train(self, X_train: pd.DataFrame, y_train: pd.DataFrame, column_names: list, type: LearningType):
        for n in range(self.n_estimators):
            tree = RandomDecisionTree(self.min_samples, np.unique(y_train), self.n_features, type)
            self.trees_.append(tree.build(deepcopy(X_train), deepcopy(y_train), deepcopy(column_names)))

        return self.trees_
